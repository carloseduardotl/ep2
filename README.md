# EP2 - OO 2019.1 (UnB - Gama)

## Sobre

O programa é capaz de armazernar veículos e criar fretes a partir deles conforme dados oferecidos pelo usuário

## Características

o programa usará esses dados como referência

- Caminhão
  - **Combustível**: Diesel
  - **Rendimento**: 8 Km/L
  - **Carga máxima**: 30 toneladas
  - **Velocidade média**: 60 Km/h
  - A cada Kg de carga, o rendimento é reduzido em 0.0002 Km/L
- Van
  - **Combustível**: Diesel
  - **Rendimento**: 10 Km/L
  - **Carga máxima**: 3,5 toneladas
  - **Velocidade média**: 80 Km/h
  - A cada Kg de carga, o rendimento é reduzido em 0.001 Km/L
- Carro
  - **Combustível**: Gasolina ou Álcool
  - **Rendimento**: 14 Km/L com gasolina, 12Km/L com álcool
  - **Carga máxima**: 360 Kg
  - **Velocidade média**: 100 Km/h
  - A cada Kg de carga, o rendimento é reduzido em 0.025 Km/L com gasolina e 0.0231 Km/L com álcool
- Moto
  - **Combustível**: Gasolina ou Álcool
  - **Rendimento**: 50 Km/L com gasolina, 43 Km/L com álcool
  - **Carga máxima**: 50 kg
  - **Velocidade média**: 110 Km/h
  - A cada Kg de carga, o rendimento é reduzido em 0.3 Km/L com gasolina e 0.4 Km/L com álcool


  - **Álcool**: R$ 3.499 por litro
  - **Gasolina**: R$ 4.449 por litro
  - **Diesel**: R$ 3.869 por litro

Serão definidos pelo usuário:

  - Margem de lucro
  - Dados do frete a ser realizado

## Sobre

O programa foi feito com o JDK 12 e Netbeans 11 como IDE  
Para executar é necessário compilar o programa




