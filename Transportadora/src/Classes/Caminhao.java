/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author cadu
 */
public class Caminhao extends Veiculo {

    public Caminhao() {
        this.setQuantidade(0);
        this.setCarga_max(30000);
        this.setVelocidade(60);
        this.setRendimento(8);
    }
    
    public float getRendimento(int carga){
        return (float) (this.getRendimento() - carga*0.0002);
    }
    
}
