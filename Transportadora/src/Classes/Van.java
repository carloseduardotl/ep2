/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author cadu
 */
public class Van extends Veiculo {

    public Van() {
        this.setQuantidade(0);
        this.setCarga_max(3500);
        this.setVelocidade(80);
        this.setRendimento(10);
    }
    
    public float getRendimento(int carga){
        return (float) (this.getRendimento() - 0.001 * carga);
    }
    
}
