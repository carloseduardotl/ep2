/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author cadu
 */
public class Moto extends Veiculo {

    public Moto() {
        this.setQuantidade(0);
        this.setCarga_max(50);
        this.setVelocidade(110);
        this.setRendimento(50);
    }
    
    public float getRendimento(String combustivel, int carga){
        if(combustivel.equals("gasolina")){
            return (float) (this.getRendimento() - 0.3 * carga);
        }
        if(combustivel.equals("alcool")){
            return (float) ((this.getRendimento()-7) - 0.4 * carga);
        }
        else
        return 0;
    }
    
}
