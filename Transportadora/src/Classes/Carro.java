/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author cadu
 */
public class Carro extends Veiculo {

    public Carro() {
        this.setQuantidade(0);
        this.setCarga_max(360);
        this.setVelocidade(100);
        this.setRendimento(14);
        
    }
    
    public float getRendimento(String combustivel, int carga){
        if(combustivel.equals("gasolina")){
            return (float) (this.getRendimento() - 0.025 * carga);
        }
        if(combustivel.equals("alcool")){
            return (float) ((this.getRendimento()-2) - 0.0231 * carga);
        }
        else
        return 0;
    }
    
    
}
