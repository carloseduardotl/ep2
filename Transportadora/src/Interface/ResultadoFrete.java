/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Arquivo.*;
import Classes.*;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author cadu
 */
public class ResultadoFrete extends javax.swing.JInternalFrame {

    Caminhao caminhoes = new Caminhao();
    Van vans = new Van();
    Carro carros = new Carro();
    Moto motos = new Moto();    
    Frete frete = new Frete();
    float custo[] = new float[6];
    float duracao[] = new float[6];
    Ler lendo = new Ler();
    Escrever escrevendo = new Escrever();
    
    DecimalFormat decimal = new DecimalFormat("#0.00");

    /**
     * Creates new form ResultadoFrete
     */
    public ResultadoFrete() {
        initComponents();
        
        
        float menorCusto = Float.MAX_VALUE;
        float menorDuracao = Float.MAX_VALUE;
        
        caminhoes.setQuantidade(lendo.lerCaminhao());
        vans.setQuantidade(lendo.lerVan());
        carros.setQuantidade(lendo.lerCarro());
        motos.setQuantidade(lendo.lerMoto());

        frete.setDistancia(lendo.lerDistancia());
        frete.setPeso(lendo.lerPeso());
        frete.setTempo(lendo.lerTempo());
        
        
        for(int i = 0; i<6; i++){
            custo[i]=Float.MAX_VALUE;
            duracao[i]=Float.MAX_VALUE;
        }
       
        
        if(frete.getPeso()>50 || ((frete.getDistancia() / (float)motos.getVelocidade())>frete.getTempo())){
            panelMotoAlcool.setVisible(false);
            panelMotoGasolina.setVisible(false);
        }
        if(frete.getPeso()>360 || ((frete.getDistancia() / (float)carros.getVelocidade())>frete.getTempo())){
            panelCarroAlcool.setVisible(false);
            panelCarroGasolina.setVisible(false);
        }
        if(frete.getPeso()>3500 || ((frete.getDistancia() / (float)vans.getVelocidade())>frete.getTempo())){
            panelVan.setVisible(false);
        }
        if(frete.getPeso()>30000 || ((frete.getDistancia()/ (float)caminhoes.getVelocidade())>frete.getTempo())){
            panelCaminhão.setVisible(false);
            
        }
        
        if(panelMotoGasolina.isVisible()){
            duracao[4]= (float) frete.getDistancia()/motos.getVelocidade();
            duracao[5]= duracao[4];
            custo[4]=(float) (((lendo.lerLucro()/100)+1)*4.449*(frete.getDistancia() / motos.getRendimento("gasolina", frete.getPeso())));
            custo[5]=(float) (((lendo.lerLucro()/100)+1)*3.499*(frete.getDistancia() / motos.getRendimento("alcool", frete.getPeso())));
            labelCMotoG.setText(decimal.format(custo[4]));
            labelCMotoA.setText(decimal.format(custo[5]));
            labelDMotoG.setText(decimal.format(duracao[4]));
            labelDMotoA.setText(decimal.format(duracao[5]));
        }
        if(panelCarroAlcool.isVisible()){
            duracao[2]=(float) frete.getDistancia()/carros.getVelocidade();
            duracao[3]= duracao[2];
            custo[2]=(float) (((lendo.lerLucro()/100)+1)*4.449*(frete.getDistancia() / carros.getRendimento("gasolina", frete.getPeso())));
            custo[3]=(float) (((lendo.lerLucro()/100)+1)*3.499*(frete.getDistancia() / carros.getRendimento("alcool", frete.getPeso())));
            labelCCarroA.setText(decimal.format(custo[3]));
            labelCCarroG.setText(decimal.format(custo[2]));
            labelDCarroA.setText(decimal.format(duracao[3]));
            labelDCarroG.setText(decimal.format(duracao[2]));
        }
        if(panelVan.isVisible()){
            duracao[1]=(float) frete.getDistancia()/vans.getVelocidade();
            custo[1]=(float) (((lendo.lerLucro()/100)+1)*3.869*(frete.getDistancia() / vans.getRendimento(frete.getPeso())));
            labelCVan.setText(decimal.format(custo[1]));
            labelDVan.setText(decimal.format(duracao[1]));
        }
        if(panelCaminhão.isVisible()){
            duracao[0]=(float) (frete.getDistancia()/caminhoes.getVelocidade());
            custo[0]=(float) (((lendo.lerLucro()/100)+1)*3.869*(frete.getDistancia() / caminhoes.getRendimento(frete.getPeso())));
            labelCCaminhao.setText(decimal.format(custo[0]));
            labelDCaminhao.setText(decimal.format(duracao[0]));
        }
        
        
        for(int i=0; i<6; i++){
            if(menorCusto>custo[i])
                menorCusto=custo[i];
            if(menorDuracao>duracao[i])
                menorDuracao=duracao[i];
        }
        
        if(menorCusto==custo[0]){
            labelMelhorCusto.setText("O Caminhão");
        }
        if(menorCusto==custo[1]){
            labelMelhorCusto.setText("A Van");
        }
        if(menorCusto==custo[2]){
            labelMelhorCusto.setText("O Carro usando gasolina");
        }
        if(menorCusto==custo[3]){
            labelMelhorCusto.setText("O Carro usando álcool");
        }
        if(menorCusto==custo[4]){
            labelMelhorCusto.setText("A Moto usando gasolina");
        }
        if(menorCusto==custo[5]){
            labelMelhorCusto.setText("A Moto usando ácool");
        }
        
        if(menorDuracao==duracao[0]){
            labelMelhorDuração.setText("O Caminhão");
        }
        if(menorDuracao==duracao[1]){
            labelMelhorDuração.setText("A Van");
        }
        if(menorDuracao==duracao[2] || menorDuracao==duracao[3]){
            labelMelhorDuração.setText("O Carro");
        }
        if(menorDuracao==duracao[4] || menorDuracao==duracao[5]){
            labelMelhorDuração.setText("A Moto");
        }
        
        LabelMelhorCB.setText(labelMelhorCusto.getText());
        
        if(!panelCaminhão.isVisible() || !panelVan.isVisible() || !panelCarroAlcool.isVisible() || !panelMotoAlcool.isVisible()){
            panelVeiculos.setVisible(false);
            panelIndicacoes.setVisible(false);
            JOptionPane.showMessageDialog(null, "Nenhum veículo é capaz de realizar esse frete");
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelVeiculos = new javax.swing.JPanel();
        panelMotoAlcool = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        labelCMotoA = new javax.swing.JLabel();
        labelDMotoA = new javax.swing.JLabel();
        botaoMotoA = new javax.swing.JButton();
        panelCaminhão = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        labelCCaminhao = new javax.swing.JLabel();
        labelDCaminhao = new javax.swing.JLabel();
        botaoCaminhao = new javax.swing.JButton();
        panelVan = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        labelCVan = new javax.swing.JLabel();
        labelDVan = new javax.swing.JLabel();
        botaoVan = new javax.swing.JButton();
        panelMotoGasolina = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        labelCMotoG = new javax.swing.JLabel();
        labelDMotoG = new javax.swing.JLabel();
        botaoMotoG = new javax.swing.JButton();
        panelCarroGasolina = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        labelCCarroG = new javax.swing.JLabel();
        labelDCarroG = new javax.swing.JLabel();
        botaoCarroG = new javax.swing.JButton();
        panelCarroAlcool = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        labelCCarroA = new javax.swing.JLabel();
        labelDCarroA = new javax.swing.JLabel();
        botaoCarroA = new javax.swing.JButton();
        panelIndicacoes = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        labelMelhorCusto = new javax.swing.JLabel();
        labelMelhorDuração = new javax.swing.JLabel();
        LabelMelhorCB = new javax.swing.JLabel();

        setClosable(true);

        panelVeiculos.setBorder(javax.swing.BorderFactory.createTitledBorder("Veículos"));

        panelMotoAlcool.setBorder(javax.swing.BorderFactory.createTitledBorder("Moto Álcool"));

        jLabel10.setText("Custo:");

        jLabel11.setText("Duração:");

        labelCMotoA.setText("100000000");

        labelDMotoA.setText("100000000");

        botaoMotoA.setText("Escolher");
        botaoMotoA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoMotoAActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMotoAlcoolLayout = new javax.swing.GroupLayout(panelMotoAlcool);
        panelMotoAlcool.setLayout(panelMotoAlcoolLayout);
        panelMotoAlcoolLayout.setHorizontalGroup(
            panelMotoAlcoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMotoAlcoolLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMotoAlcoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMotoAlcoolLayout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelCMotoA))
                    .addGroup(panelMotoAlcoolLayout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelDMotoA)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMotoAlcoolLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(botaoMotoA))
        );
        panelMotoAlcoolLayout.setVerticalGroup(
            panelMotoAlcoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMotoAlcoolLayout.createSequentialGroup()
                .addComponent(botaoMotoA)
                .addGap(1, 1, 1)
                .addGroup(panelMotoAlcoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(labelCMotoA))
                .addGap(18, 18, 18)
                .addGroup(panelMotoAlcoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(labelDMotoA))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelCaminhão.setBorder(javax.swing.BorderFactory.createTitledBorder("Caminhão"));

        jLabel1.setText("Custo:");

        jLabel2.setText("Duração:");

        labelCCaminhao.setText("100000000");

        labelDCaminhao.setText("100000000");

        botaoCaminhao.setText("Escolher");
        botaoCaminhao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCaminhaoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelCaminhãoLayout = new javax.swing.GroupLayout(panelCaminhão);
        panelCaminhão.setLayout(panelCaminhãoLayout);
        panelCaminhãoLayout.setHorizontalGroup(
            panelCaminhãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCaminhãoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCaminhãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCaminhãoLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelCCaminhao))
                    .addGroup(panelCaminhãoLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelDCaminhao)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCaminhãoLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(botaoCaminhao))
        );
        panelCaminhãoLayout.setVerticalGroup(
            panelCaminhãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCaminhãoLayout.createSequentialGroup()
                .addComponent(botaoCaminhao)
                .addGap(1, 1, 1)
                .addGroup(panelCaminhãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(labelCCaminhao))
                .addGap(18, 18, 18)
                .addGroup(panelCaminhãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(labelDCaminhao))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelVan.setBorder(javax.swing.BorderFactory.createTitledBorder("Van"));

        jLabel4.setText("Custo:");

        jLabel5.setText("Duração:");

        labelCVan.setText("100000000");

        labelDVan.setText("100000000");

        botaoVan.setText("Escolher");
        botaoVan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoVanActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelVanLayout = new javax.swing.GroupLayout(panelVan);
        panelVan.setLayout(panelVanLayout);
        panelVanLayout.setHorizontalGroup(
            panelVanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVanLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelVanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelVanLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelCVan))
                    .addGroup(panelVanLayout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelDVan)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelVanLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(botaoVan))
        );
        panelVanLayout.setVerticalGroup(
            panelVanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVanLayout.createSequentialGroup()
                .addComponent(botaoVan)
                .addGap(1, 1, 1)
                .addGroup(panelVanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(labelCVan))
                .addGap(18, 18, 18)
                .addGroup(panelVanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(labelDVan))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelMotoGasolina.setBorder(javax.swing.BorderFactory.createTitledBorder("Moto Gasolina"));

        jLabel12.setText("Custo:");

        jLabel13.setText("Duração:");

        labelCMotoG.setText("100000000");

        labelDMotoG.setText("100000000");

        botaoMotoG.setText("Escolher");
        botaoMotoG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoMotoGActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMotoGasolinaLayout = new javax.swing.GroupLayout(panelMotoGasolina);
        panelMotoGasolina.setLayout(panelMotoGasolinaLayout);
        panelMotoGasolinaLayout.setHorizontalGroup(
            panelMotoGasolinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMotoGasolinaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMotoGasolinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMotoGasolinaLayout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelCMotoG))
                    .addGroup(panelMotoGasolinaLayout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelDMotoG)))
                .addContainerGap(56, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMotoGasolinaLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(botaoMotoG))
        );
        panelMotoGasolinaLayout.setVerticalGroup(
            panelMotoGasolinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMotoGasolinaLayout.createSequentialGroup()
                .addComponent(botaoMotoG)
                .addGap(1, 1, 1)
                .addGroup(panelMotoGasolinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(labelCMotoG))
                .addGap(18, 18, 18)
                .addGroup(panelMotoGasolinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(labelDMotoG))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelCarroGasolina.setBorder(javax.swing.BorderFactory.createTitledBorder("Carro Gasolina"));

        jLabel6.setText("Custo:");

        jLabel7.setText("Duração:");

        labelCCarroG.setText("100000000");

        labelDCarroG.setText("100000000");

        botaoCarroG.setText("Escolher");
        botaoCarroG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCarroGActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelCarroGasolinaLayout = new javax.swing.GroupLayout(panelCarroGasolina);
        panelCarroGasolina.setLayout(panelCarroGasolinaLayout);
        panelCarroGasolinaLayout.setHorizontalGroup(
            panelCarroGasolinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCarroGasolinaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCarroGasolinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCarroGasolinaLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelCCarroG))
                    .addGroup(panelCarroGasolinaLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelDCarroG)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCarroGasolinaLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(botaoCarroG))
        );
        panelCarroGasolinaLayout.setVerticalGroup(
            panelCarroGasolinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCarroGasolinaLayout.createSequentialGroup()
                .addComponent(botaoCarroG)
                .addGap(1, 1, 1)
                .addGroup(panelCarroGasolinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(labelCCarroG))
                .addGap(18, 18, 18)
                .addGroup(panelCarroGasolinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(labelDCarroG))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelCarroAlcool.setBorder(javax.swing.BorderFactory.createTitledBorder("Carro Álcool"));

        jLabel8.setText("Custo:");

        jLabel9.setText("Duração:");

        labelCCarroA.setText("100000000");

        labelDCarroA.setText("100000000");

        botaoCarroA.setText("Escolher");
        botaoCarroA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCarroAActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelCarroAlcoolLayout = new javax.swing.GroupLayout(panelCarroAlcool);
        panelCarroAlcool.setLayout(panelCarroAlcoolLayout);
        panelCarroAlcoolLayout.setHorizontalGroup(
            panelCarroAlcoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCarroAlcoolLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCarroAlcoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCarroAlcoolLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelCCarroA))
                    .addGroup(panelCarroAlcoolLayout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelDCarroA)))
                .addContainerGap(56, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCarroAlcoolLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(botaoCarroA))
        );
        panelCarroAlcoolLayout.setVerticalGroup(
            panelCarroAlcoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCarroAlcoolLayout.createSequentialGroup()
                .addComponent(botaoCarroA)
                .addGap(1, 1, 1)
                .addGroup(panelCarroAlcoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(labelCCarroA))
                .addGap(18, 18, 18)
                .addGroup(panelCarroAlcoolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(labelDCarroA))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelVeiculosLayout = new javax.swing.GroupLayout(panelVeiculos);
        panelVeiculos.setLayout(panelVeiculosLayout);
        panelVeiculosLayout.setHorizontalGroup(
            panelVeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelVeiculosLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelVeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelMotoGasolina, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelCarroGasolina, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelCaminhão, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelCarroAlcool, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelVan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelMotoAlcool, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelVeiculosLayout.setVerticalGroup(
            panelVeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelVeiculosLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelVeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelVan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelCaminhão, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelVeiculosLayout.createSequentialGroup()
                        .addComponent(panelCarroAlcool, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelMotoAlcool, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelVeiculosLayout.createSequentialGroup()
                        .addComponent(panelCarroGasolina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelMotoGasolina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        panelIndicacoes.setBorder(javax.swing.BorderFactory.createTitledBorder("Indicações"));

        jLabel3.setText("Melhor custo:");

        jLabel14.setText("Menor duração:");

        jLabel15.setText("Melhor C/B:");

        labelMelhorCusto.setText("jLabel16");

        labelMelhorDuração.setText("jLabel16");

        LabelMelhorCB.setText("jLabel16");

        javax.swing.GroupLayout panelIndicacoesLayout = new javax.swing.GroupLayout(panelIndicacoes);
        panelIndicacoes.setLayout(panelIndicacoesLayout);
        panelIndicacoesLayout.setHorizontalGroup(
            panelIndicacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelIndicacoesLayout.createSequentialGroup()
                .addGroup(panelIndicacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelIndicacoesLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelMelhorCusto))
                    .addGroup(panelIndicacoesLayout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelMelhorDuração))
                    .addGroup(panelIndicacoesLayout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LabelMelhorCB)))
                .addGap(0, 70, Short.MAX_VALUE))
        );
        panelIndicacoesLayout.setVerticalGroup(
            panelIndicacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelIndicacoesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelIndicacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(labelMelhorCusto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelIndicacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(labelMelhorDuração))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelIndicacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(LabelMelhorCB))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelVeiculos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelIndicacoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelIndicacoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelVeiculos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botaoCaminhaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCaminhaoActionPerformed
        // TODO add your handling code here:
        if(lendo.lerCaminhao()>0){
            escrevendo.armazenar((lendo.lerCaminhao()-1), lendo.lerVan(), lendo.lerCarro(), lendo.lerMoto());
            this.dispose();
            JOptionPane.showMessageDialog(null, "Frete adicionado");
        }
        else{
            JOptionPane.showMessageDialog(null, "Não há caminhões disponíveis");
        }
    }//GEN-LAST:event_botaoCaminhaoActionPerformed

    private void botaoVanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoVanActionPerformed
        // TODO add your handling code here:
        if(lendo.lerVan()>0){
            escrevendo.armazenar(lendo.lerCaminhao(), (lendo.lerVan()-1), lendo.lerCarro(), lendo.lerMoto());
            this.dispose();
            JOptionPane.showMessageDialog(null, "Frete adicionado");
        }
        else{
            JOptionPane.showMessageDialog(null, "Não há vans disponíveis");
        }       
    }//GEN-LAST:event_botaoVanActionPerformed

    private void botaoCarroGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCarroGActionPerformed
        // TODO add your handling code here:
        if(lendo.lerCarro()>0){
            escrevendo.armazenar(lendo.lerCaminhao(), lendo.lerVan(), (lendo.lerCarro()-1), lendo.lerMoto());
            this.dispose();
            JOptionPane.showMessageDialog(null, "Frete adicionado");
        }
        else{
            JOptionPane.showMessageDialog(null, "Não há carros disponíveis");
        }
    }//GEN-LAST:event_botaoCarroGActionPerformed

    private void botaoCarroAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCarroAActionPerformed
        // TODO add your handling code here:
        if(lendo.lerCarro()>0){
            escrevendo.armazenar(lendo.lerCaminhao(), lendo.lerVan(), (lendo.lerCarro()-1), lendo.lerMoto());
            this.dispose();
            JOptionPane.showMessageDialog(null, "Frete adicionado");
        }
        else{
            JOptionPane.showMessageDialog(null, "Não há carros disponíveis");
        }
    }//GEN-LAST:event_botaoCarroAActionPerformed

    private void botaoMotoGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoMotoGActionPerformed
        // TODO add your handling code here:
        if(lendo.lerMoto()>0){
            escrevendo.armazenar(lendo.lerCaminhao(), lendo.lerVan(), lendo.lerCarro(), (lendo.lerMoto()-1));
            this.dispose();
            JOptionPane.showMessageDialog(null, "Frete adicionado");

        }
        else{
            JOptionPane.showMessageDialog(null, "Não há motos disponíveis");
        }
    }//GEN-LAST:event_botaoMotoGActionPerformed

    private void botaoMotoAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoMotoAActionPerformed
        // TODO add your handling code here:
        if(lendo.lerMoto()>0){
            escrevendo.armazenar(lendo.lerCaminhao(), lendo.lerVan(), lendo.lerCarro(), (lendo.lerMoto()-1));
            this.dispose();
            JOptionPane.showMessageDialog(null, "Frete adicionado");
        }
        else{
            JOptionPane.showMessageDialog(null, "Não há motos disponíveis");
        }
    }//GEN-LAST:event_botaoMotoAActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelMelhorCB;
    private javax.swing.JButton botaoCaminhao;
    private javax.swing.JButton botaoCarroA;
    private javax.swing.JButton botaoCarroG;
    private javax.swing.JButton botaoMotoA;
    private javax.swing.JButton botaoMotoG;
    private javax.swing.JButton botaoVan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel labelCCaminhao;
    private javax.swing.JLabel labelCCarroA;
    private javax.swing.JLabel labelCCarroG;
    private javax.swing.JLabel labelCMotoA;
    private javax.swing.JLabel labelCMotoG;
    private javax.swing.JLabel labelCVan;
    private javax.swing.JLabel labelDCaminhao;
    private javax.swing.JLabel labelDCarroA;
    private javax.swing.JLabel labelDCarroG;
    private javax.swing.JLabel labelDMotoA;
    private javax.swing.JLabel labelDMotoG;
    private javax.swing.JLabel labelDVan;
    private javax.swing.JLabel labelMelhorCusto;
    private javax.swing.JLabel labelMelhorDuração;
    private javax.swing.JPanel panelCaminhão;
    private javax.swing.JPanel panelCarroAlcool;
    private javax.swing.JPanel panelCarroGasolina;
    private javax.swing.JPanel panelIndicacoes;
    private javax.swing.JPanel panelMotoAlcool;
    private javax.swing.JPanel panelMotoGasolina;
    private javax.swing.JPanel panelVan;
    private javax.swing.JPanel panelVeiculos;
    // End of variables declaration//GEN-END:variables
}
