/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arquivo;

import java.io.*;

/**
 *
 * @author cadu
 */
public class Ler {
   
    public static String mostraLinha(int line_number){
        try{
            BufferedReader br_file=new BufferedReader(new FileReader ("dados.txt"));
            String linha;
            linha = " ";
            int i=0;
            while((linha=br_file.readLine())!=null){
                if(i==line_number){
                    return linha;
                }
                i++;
            }
        }catch(IOException e){
            System.exit(1);
        }
        return null;
    }
    
    public int lerCaminhao()
    {
        String linha;
        linha = Ler.mostraLinha(0);
        return Integer.valueOf(linha);
    }
    
    public int lerVan()
    {
        String linha;
        linha = Ler.mostraLinha(1);
        return Integer.valueOf(linha);
    }
    
    public int lerCarro()
    {
        String linha;
        linha = Ler.mostraLinha(2);
        return Integer.valueOf(linha);
    }
    
    public int lerMoto()
    {
        String linha;
        linha = Ler.mostraLinha(3);
        return Integer.valueOf(linha);
    }
    
    public static String mostraLinhaLucro(int line_number){
        try{
            BufferedReader br_file=new BufferedReader(new FileReader ("lucro.txt"));
            String linha;
            linha = " ";
            int i=0;
            while((linha=br_file.readLine())!=null){
                if(i==line_number){
                    return linha;
                }
                i++;
            }
        }catch(IOException e){
            System.exit(1);
        }
        return null;
    }
    
    public float lerLucro(){
        String linha;
        linha=Ler.mostraLinhaLucro(0);
        return Float.parseFloat(linha);
    }
    
    public static String mostraLinhaFrete(int line_number){
        try{
            BufferedReader br_file=new BufferedReader(new FileReader ("frete.txt"));
            String linha;
            linha = " ";
            int i=0;
            while((linha=br_file.readLine())!=null){
                if(i==line_number){
                    return linha;
                }
                i++;
            }
        }catch(IOException e){
            System.exit(1);
        }
        return null;
    }
    
    public int lerDistancia(){
        String linha;
        linha=Ler.mostraLinhaFrete(0);
        return Integer.valueOf(linha);
    }
    
    public int lerPeso(){
        String linha;
        linha=Ler.mostraLinhaFrete(1);
        return Integer.valueOf(linha);
    }
    
    public int lerTempo(){
        String linha;
        linha=Ler.mostraLinhaFrete(2);
        return Integer.valueOf(linha);
    }
}
