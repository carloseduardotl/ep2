/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arquivo;

import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.FormatterClosedException;
import java.lang.SecurityException;
import java.io.FileNotFoundException;

/**
 *
 * @author cadu
 */
public class Escrever {
    private Formatter frota;
    private Formatter lucro;
    private Formatter frete;

        public void abrirFrota(){
            try
            {
                frota = new Formatter("dados.txt");
            }
            catch( SecurityException semPermissao)
            {
                System.err.println(" Sem permissao para escrever no arquivo");
                System.exit(1); 
            }
            catch( FileNotFoundException arquivoInexistente )
            {
                System.err.println(" Arquivo inexistente ou arquivo não pode ser criado");
                System.exit(1);
            }
        }
         
        public void armazenar(int caminhao, int van, int carro, int moto){
            this.abrirFrota();
            try
            {
                frota.format(String.valueOf(caminhao)+"\n"+String.valueOf(van)+"\n"+String.valueOf(carro)+"\n"+String.valueOf(moto));
            }
            catch(FormatterClosedException formatoDesconhecido)
            {
                        System.err.println("Erro ao escrever");
            }
            catch(NoSuchElementException excecaoElemento)
            {
                System.err.println("Entrada invalida");
            }
            this.fecharFrota();
        }
     
       public void fecharFrota(){
           frota.close();
       }
       
       
       public void abrirLucro(){
            try
            {
                lucro = new Formatter("lucro.txt");
            }
            catch( SecurityException semPermissao)
            {
                System.err.println(" Sem permissao para escrever no arquivo");
                System.exit(1); 
            }
            catch( FileNotFoundException arquivoInexistente )
            {
                System.err.println(" Arquivo inexistente ou arquivo não pode ser criado");
                System.exit(1);
            }
        }
       
       public void fecharLucro(){
           lucro.close();
       }
       
       public void armazenar(float margemLucro){
            this.abrirLucro();
            try
            {
                lucro.format(String.valueOf(margemLucro));
            }
            catch(FormatterClosedException formatoDesconhecido)
            {
                        System.err.println("Erro ao escrever");
            }
            catch(NoSuchElementException excecaoElemento)
            {
                System.err.println("Entrada invalida");
            }
            this.fecharLucro();
        }
       
       public void abrirFrete(){
            try
            {
                frete = new Formatter("frete.txt");
            }
            catch( SecurityException semPermissao)
            {
                System.err.println(" Sem permissao para escrever no arquivo");
                System.exit(1); 
            }
            catch( FileNotFoundException arquivoInexistente )
            {
                System.err.println(" Arquivo inexistente ou arquivo não pode ser criado");
                System.exit(1);
            }
        }
       
       public void fecharFrete(){
           frete.close();
       }
       
       public void armazenar(int dist, int peso, int tempo){
            this.abrirFrete();
            try
            {
                frete.format(String.valueOf(dist)+"\n"+String.valueOf(peso)+"\n"+String.valueOf(tempo));
            }
            catch(FormatterClosedException formatoDesconhecido)
            {
                        System.err.println("Erro ao escrever");
            }
            catch(NoSuchElementException excecaoElemento)
            {
                System.err.println("Entrada invalida");
            }
            this.fecharFrete();
        }
}
